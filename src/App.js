import React, {Component} from 'react';
import Navigation from './components/Navigation/Navigation';
import Logo from './components/Logo/Logo';
import FaceRecognition from './components/FaceRecognition/FaceRecognition';
import ImageLinkForm from './components/ImageLinkForm/ImageLinkForm';
import Signin from './components/Signin/Signin';
import Register from './components/Register/Register';
import Rank from './components/Rank/Rank';
import Particles from 'react-particles-js'
import Clarifai from 'clarifai';
import './App.css';


const END_POINT = (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1" || window.location.hostname === "") ?
"http://localhost:3003/":"https://stormy-inlet-94280.herokuapp.com/";

/* TEST PICS: 
https://images2.minutemediacdn.com/image/upload/c_fill,w_912,h_516,f_auto,q_auto,g_auto/shape/cover/sport/arsenal-v-stade-rennais-uefa-europa-league-round-of-16-second-leg-5d10950c96ebdce4bb000012.jpg
https://www.loverugbyleague.com/content/uploads/2019/06/Australia-2017-World-Cup-winners-PA-678x381.png
*/
const particlesOps= 
{
  "particles": {
      "line_linked": {
                  "color":"#FFFFFF"
                  },
      "number": {
          "value": 150
      },
      "size": {
          "value": 5
      }
  },
  "interactivity": {
      "events": {
          "onhover": {
              "enable": true,
              "mode": "repulse"
          }
      }
  }
};

const app = new Clarifai.App({
  apiKey: 'f026ea62fbd04b0fa7230c6043dc2b4c'
});

const initialState= {
  input: '',
  imageUrl: '',
  box: [],
  route: 'signin',
  isSignedIn: false,
  envURL: END_POINT,
  user: {
    id: '',
    name: '',
    email: '',
    entries: {
        football: 0,
        rugbyl: 0,
        rugbyu: 0,
        unknown: 0
    },
    joined: ''
  }
};

class App extends Component {

  constructor() {
    super();
    this.state = initialState;
  }

  loadUser = (data) => {
    this.setState({user:
      {
        id: data.id,
        name: data.name,
        email: data.email,
        entries: {
            football: data.football,
            rugbyl: data.rugbyl,
            rugbyu: data.rugbyu,
            unknown: data.unknown,
        },
        joined: data.joined
      }
    })
  }

  getAllFacesInPic = (data) => {
    const clarifaiFaces = data.outputs[0].data.regions;
    const image = document.getElementById('inputImage');
    const width = Number(image.width);
    const height = Number(image.height);
    return clarifaiFaces.map( face => {
      const cf= face.region_info.bounding_box;
      return {
        k: face.id,
        l: cf.left_col * width,
        t: cf.top_row * height,
        r: width - (cf.right_col * width),
        b: height - (cf.bottom_row * height)
      }
    });
  }

  updateEntries = (pid)=>{
    fetch(this.state.envURL+'image',{
      method: 'put',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        userid: this.state.user.id,
        phototypeid: pid
      })
    })
    .then(response => response.json())
    .then(count => {
      switch(pid){
        case "fb": this.setState(
          Object.assign(this.state.user.entries,{football: count})
        ); break;
        case "rl": this.setState(
          Object.assign(this.state.user.entries,{rugbyl: count})
        ); break;
        case "ru": this.setState(
          Object.assign(this.state.user.entries,{rugbyu: count})
        ); break;
        case "uk": this.setState(
          Object.assign(this.state.user.entries,{unknown: count})
        ); break;
        default: break;
      }
    })
  }

  shout = (huh) => {
    let response= "";
    let phototypeid= "uk";
    switch(this.state.box.length){
      case 11: phototypeid= "fb"; response= "Its a football team"; break;
      case 13: phototypeid= "rl"; response= "Its a rugby league team"; break;
      case 15: phototypeid= "ru"; response= "Its a rugby union team"; break;
      default: phototypeid= "uk"; response= "No idea, dogg?!"; break;
    }
    this.updateEntries(phototypeid);
    alert(response);
  }

  displayFaceBox = (box) => {
    this.setState({ box: box });
  }

  onInputChange = (event) => {
    this.setState({input: event.target.value});
  }

  onButtonSubmit = (event) => {
    this.setState({imageUrl: this.state.input});
    app.models.predict(
      Clarifai.FACE_DETECT_MODEL, 
      this.state.input)
    .then(response => {

      this.shout(this.displayFaceBox(this.getAllFacesInPic(response)));
    })
    .catch(err => console.log(err));
  }

  onRouteChange = (route) => {
    if(route === 'signout'){
      this.setState(initialState);
    }else if(route === 'home'){
      this.setState({isSignedIn: true});
    }
    this.setState({route: route});
  }
  

  displayPageInView = (routeNeeded) => {
    switch(routeNeeded){
      case 'home':
        return(<div>
          <Logo />
          <Rank name={this.state.user.name} entries={this.state.user.entries} />
          <ImageLinkForm onInputChange={this.onInputChange} onButtonSubmit={this.onButtonSubmit} />
          <FaceRecognition box={this.state.box} imageUrl={this.state.imageUrl} />        
        </div>);
      case 'register': 
        return(<Register envURL={this.state.envURL} loadUser={this.loadUser} onRouteChange={this.onRouteChange} />);
      case 'signin': 
      default:
          return(<Signin envURL={this.state.envURL} loadUser={this.loadUser} onRouteChange={this.onRouteChange} />);
    }
  }


  render(){
    return (
      <div className="App">
        <Particles className="particles" params={particlesOps} />
        <Navigation isSignedIn={this.state.isSignedIn} onRouteChange={this.onRouteChange} />
        {this.displayPageInView(this.state.route)}
      </div>
    );
  }
}

export default App;
