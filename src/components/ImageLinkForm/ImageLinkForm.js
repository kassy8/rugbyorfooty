import React from 'react';
import './ImageLinkForm.css';

const ImageLinkForm = ({onInputChange,onButtonSubmit}) => {
    return(
        <div>
            <p className="f3">
            {'This Rugby or Football app will detect whether a chosen picture is of a Rubgy League, Union or Football team'}
            </p>
            <div className='center'>
                <div className='pa4 br3 shadow-5 form center'>
                    <input onChange={onInputChange} className='f4 pa2 w-70 center' type="text" />
                    <button onClick={onButtonSubmit} className='w-30 grow f4 link ph3 pv2 dib white bg-light-purple'>Detection Ting!</button>
                </div>
            </div>
        </div>
    );
}

export default ImageLinkForm;