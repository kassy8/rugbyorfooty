import React from 'react';
import './FaceRecognition.css'

// alt={imgAlt}




const FaceRecognition = ({imageUrl, box}) => {

    const showBox= (f) => {
        return <div key={f.k} className='bounding-box' style={{top:f.t,right:f.r,bottom:f.b,left:f.l}}></div>
    }

    return(
        <div className="center ma">
            <div className="absolute mt2">
                <img id='inputImage' src={imageUrl} alt="" width="500px" height="auto" />
                { box.map(f=>{ return showBox(f); }) }
            </div>
        </div>
    )
}

export default FaceRecognition;