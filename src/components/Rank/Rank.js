import React from 'react';

const Rank = ({name, entries}) => {
    const {football, rugbyl, rugbyu, unknown} = entries;
    return(
        <div>
            <div className='white f3'>
                <div className='white f5'>
                {`${name} , your current rank is...`}
                <br />
                {`${football} , for football`}
                <br />
                {`${rugbyl} , for rug league`}
                <br />
                {`${rugbyu} , for rug union and `}
                <br />
                {`${unknown} , for unknown`}
                </div>
            </div>
        </div>
    );
}

export default Rank;